from django.db import models

class PortfolioItem(models.Model):
    """
    Portfolio
    """
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='uploads', null=True, blank=True)
    description = models.TextField()
    url = models.URLField(null=True, blank=True)

    def __str__(self, ):
        return self.title
