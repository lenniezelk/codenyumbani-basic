from django.shortcuts import render
from django.views.generic.base import View

from . import models

class PortfolioHomeView(View):
    """
    Portfolio Home
    """
    def get(self, request):
        portfolio_items = models.PortfolioItem.objects.all()

        template_values = {
                    'portfolio_items': portfolio_items
                }

        return render(request, 'portfolio/home.html', template_values)
