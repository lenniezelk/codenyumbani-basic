from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns(
        '',
        url(r'^$', views.PortfolioHomeView.as_view(), name='portfolio-home')
        )
