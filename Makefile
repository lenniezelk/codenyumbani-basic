env=development
process=all
group=all

ANSIBLE = ansible $(group) -i devops/inventory/$(env)
ANSIBLE_PLAYBOOK = ansible-playbook -i devops/inventory/$(env)

# Match any playbook in the devops directory
% :: devops/%.yml 
	$(ANSIBLE_PLAYBOOK) $< -l $(group)
	
staging :
	$(eval env = staging )
	$(ANSIBLE_PLAYBOOK) devops/deploy.yml

status :
	$(ANSIBLE) -s -a "sudo monit status" 

restart-all :
	$(ANSIBLE) -s -a "monit restart all" 

restart-process:
	$(ANSIBLE) -s -a "monit restart -g $(process)"

restart-monit :
	ansible app_servers -i devops/inventory/$(env) -m shell -s \
	-a "service monit restart"


help:
	@echo ''
	@echo 'Usage: '
	@echo ' make <command> [option=<option_value>]...'
	@echo ''
	@echo 'Setup & Deployment:'
	@echo ' make configure		Prepare servers'
	@echo ' make deploy 		Deploy app'
	@echo ''
	@echo 'Options:  '
	@echo ' env			Inventory file (Default: development)'
	@echo ' group			Inventory subgroup (Default: all)'
	@echo ''
	@echo 'Example:'
	@echo ' make configure env=staging group=app_servers'
	@echo ''
	@echo 'Application Management:'
	@echo ' make status 		Display process states'
	@echo ' make start		Start processes'
	@echo ' make restart		Restart processes'
	@echo ' make restart-supervisor	Restart supervisord'
	@echo ''
	@echo 'Options: '
	@echo ' process		A supervisor program name or group (Default: all)'
	@echo ''
	@echo 'Example:'


test:
	$(ANSIBLE) -m django_manage -a "command=test app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/cashbox settings=fsite.settings.development apps='frontend cashbox'"
	$(ANSIBLE) -m django_manage -a "command=test app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/cashbox settings=fsite.settings.allauth_test apps='allauth.socialaccount allauth.account'"
	
makemessages_js:
	$(ANSIBLE) -m django_manage -a "command='makemessages -d djangojs -l de' app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/cashbox settings=fsite.settings.development"
	
makemessages:
	$(ANSIBLE) -m django_manage -a "command='makemessages -l de' app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/cashbox settings=fsite.settings.development"
	
compilemessages:
	$(ANSIBLE) -m django_manage -a "command=compilemessages app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/cashbox settings=fsite.settings.development"

collectstatic:
	$(ANSIBLE) -m django_manage -a "command='collectstatic --noinput' app_path=/vagrant virtualenv=/home/vagrant/.virtualenvs/codenyumbani settings=codenyumbani.settings.development"
