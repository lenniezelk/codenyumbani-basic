from django.conf import urls
from django.views.generic import TemplateView

from .views import BlogDetailView, BlogHomeView

urlpatterns = urls.patterns(
            '',
            urls.url(
                r'^$',
                BlogHomeView.as_view(),
                name='blogs-home'
                ),
            urls.url(
                r'^(?P<slug>[a-zA-Z0-9-]+)/?$',
                BlogDetailView.as_view(),
                name='blog-detail'
                )
        )
