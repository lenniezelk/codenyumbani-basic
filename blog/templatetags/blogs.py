from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
import markdown as md

register = template.Library()

@register.filter(needs_autoescape=True)
def markdown(value, autoescape=True):
    """
    Convert value to HTML
    """
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x

    result = md.markdown(value)

    result = esc(result)

    return mark_safe(result)
