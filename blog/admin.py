from django.contrib import admin

from . import models

class PostAdmin(admin.ModelAdmin):
    """
    Post admin
    """
    list_display = ('slug', 'created_at', 'published')
    list_editable = ('published',)
    prepopulated_fields = {'slug': ('title', )}

admin.site.register(models.Post, PostAdmin)
