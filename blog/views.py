from django.shortcuts import get_object_or_404, render
from django.views.generic import View

from . import models as blog_models

class BlogHomeView(View):
    """
    Home View
    """
    def get(self, request):
        posts = blog_models.Post.objects.filter(published=True)

        template_values = {
                    'posts': posts
                }

        return render(request, 'blog/home.html', template_values)

class BlogDetailView(View):
    """
    Blog detail view
    """
    def get(self, request, slug):
        post = get_object_or_404(blog_models.Post, slug=slug, published=True)

        template_values = {
                    'post': post
                }

        return render(request, 'blog/detail.html', template_values)
