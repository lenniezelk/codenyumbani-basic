import importlib
import os

env = os.environ.get('CODENYUMBANI_ENV', 'base')

module = importlib.__import__('{}'.format(env), globals(), locals(), level=1)

module_dict = module.__dict__

try:
    to_import = module_dict.__all__
except AttributeError:
    to_import = [name for name in module_dict if not name.startswith('_')]

globals().update({name: module_dict[name] for name in to_import})
