# Codenyumbani #



### Repository for my personal website ###

####Tools in use####

* Vagrant
* Ansible
* Make

##Setting up the development environment
Ensure you have a *nix environment. Ansible doesn't currently support Windows.

* Install the tools listed at the start of this document
* Generate SSH keys by running: `ssh-keygen`
* Ensure an  ssh-agent manager is installed on your development machine e.g http://linux.die.net/man/1/keychain
* To your SSH config file add:
```
Host vagrant 
  HostName 127.0.0.1
  User vagrant
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentitiesOnly yes
  LogLevel FATAL
  ForwardAgent yes
```
* Clone the project: `git clone git@bitbucket.org:lenniezelk/codenyumbani-basic.git`
* `cd` into the project root
* Run `vagrant up`
* On your browser go to `http://localhost:8080`