from django.shortcuts import render
from django.views.generic import View

from blog import models as blog_models

class HomeView(View):
    """
    Home View
    """
    def get(self, request):
        posts = blog_models.Post.objects.filter(published=True)[:5]

        template_values = {
                    'posts': posts
                }

        return render(request, 'core/home.html', template_values)
