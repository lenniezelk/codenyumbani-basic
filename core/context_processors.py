def menu(request):
    """
    Determine from request path the active page
    """
    data = {}

    if request.path.startswith('/blog'):
        data['is_blog'] = True
    elif request.path.startswith('/portfolio'):
        data['is_portfolio'] = True
    elif request.path.startswith('/contact'):
        data['is_contact'] = True
    else:
        data['is_home'] = True

    return data
