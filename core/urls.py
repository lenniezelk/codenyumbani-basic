from django.conf import urls
from django.views.generic import TemplateView

from .views import HomeView

urlpatterns = urls.patterns(
            '',
            urls.url(
                    r'^$',
                    HomeView.as_view(),
                    name='core-home'
                )
        )
