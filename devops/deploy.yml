---
- hosts: app_servers
  gather_facts: no

  tasks:
    - name: Check out code on app server
      git: >
        repo={{ APP_REPOSITORY }} 
        dest={{ APP_DIR }} 
        version={{ APP_VERSION }}
        accept_hostkey=yes
      when: APP_ENVIRONMENT != "development"
      notify: 
          - restart app

    - name: Ensure runtime directories
      file: path={{ APP_DIR }}/{{ item }} state=directory 
      with_items:
        - log
        - pids

    - name: Install application requirements
      # Due to a bug in Ansible local connection mode, have to specify
      # app user as sudo user ("sudo: no" has no effect). 
      pip: >
        requirements={{ APP_DIR }}/requirements/{{ APP_ENVIRONMENT }}.txt
        virtualenv={{ APP_VIRTUALENV }}
        executable=pip3
      tags:
        - pip
        
    - name: Management Commands
      django_manage: >
        app_path={{ APP_DIR }} 
        virtualenv={{ APP_VIRTUALENV }}
        settings=codenyumbani.settings.{{ APP_ENVIRONMENT }}
        command={{ item }} 
      with_items:
        - validate
        - migrate
        - collectstatic

    - name: Install Node application requirements
      npm: >
        global=yes
        state=present
        name={{ item.value.name }}
        version={{ item.value.version }}
      with_dict: npm_packages
      sudo: yes
      
    - name: Write Gunicorn Monit configuration file
      template: src=templates/gunicorn_monit.j2 dest=/etc/monit/conf.d/gunicorn
      sudo: yes
      notify: restart Monit

    - name: Write Development Gunicorn file
      template: src=templates/gunicorn_config.j2 dest="{{ APP_DIR }}/gunicorn_config.py"
      notify: restart app

    - name: Write Nginx Monit configuration file
      template: src=templates/nginx_monit.j2 dest=/etc/monit/conf.d/nginx
      sudo: yes
      notify: restart Monit

  handlers:
    - name: restart Monit
      service: name=monit state=restarted
      sudo: yes

    - name: restart app 
      command: monit restart -g gunicorn 
      sudo: yes
